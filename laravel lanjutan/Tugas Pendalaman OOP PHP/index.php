<?php

    abstract class Hewan{
        use Fight;
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi(){
            echo "$this->nama sedang $this->keahlian";
        }

        abstract public function getInfoHewan();

    }

    trait Fight{
        public $attackPower;
        public $defencePower;

        public function serang(Hewan $hewan1, Hewan $hewan2)
        {
            echo "$hewan1->nama sedang menyerang $hewan2->nama \n";
            $this->diserang($hewan2,$hewan1);
        }

        public function diserang(Hewan $hewan2, Hewan $hewan1)
        {
            echo "$hewan2->nama sedang sedang diserang oleh $hewan1->nama \n";
            $hewan2->darah = $hewan2->darah - ($hewan1->attackPower/$hewan2->defencePower);
            echo "$hewan2->nama darah sisa $hewan2->darah \n";
            if($hewan2->darah<0)
                echo "\n $hewan1->nama memenangkan pertarungan";
        }
    }

    class Harimau extends Hewan{
        public function __construct($name)
        {
            $this->nama = $name;
            $this->jumlahKaki = 4;
            $this->keahlian = 'lari cepat';
            $this->attackPower = 7;
            $this->defencePower = 8;
        }

        public function getInfoHewan()
        {
            echo "Nama :$this->nama \nJumlah kaki: $this->jumlahKaki \nKeahlian: $this->keahlian \nAttack Power: $this->attackPower \nDefence Power: $this->defencePower";
        }
    }

    class Elang extends Hewan{
        public function __construct($name)
        {
            $this->nama = $name;
            $this->jumlahKaki = 2;
            $this->keahlian = 'terbang tinggi';
            $this->attackPower = 10;
            $this->defencePower = 5;
        }

        public function getInfoHewan()
        {
            echo "\nNama :$this->nama \nJumlah kaki: $this->jumlahKaki \nKeahlian: $this->keahlian \nAttack Power: $this->attackPower \nDefence Power: $this->defencePower";
        }
    }

    $elang1 = new Elang("elang1");
    $harimau5 = new Harimau("harimau5");

    $i = 0;
    while($harimau5->darah>0){
        $i++;
        echo "\nRonde ke - $i \n";
        $elang1->serang($elang1,$harimau5);
    }

    $elang1->getInfoHewan();
?>