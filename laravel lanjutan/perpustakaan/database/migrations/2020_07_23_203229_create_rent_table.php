<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('rentDate');
            $table->date('dueDate');            
            $table->date('returnDate')->nullable();
            $table->tinyInteger('onDate')->default(0);
            $table->integer('borrower_id');
            $table->foreign('borrower_id')->references('id')->on('users');
            $table->integer('admin_id');
            $table->foreign('admin_id')->references('id')->on('users');
            $table->timestamps();            
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent');
    }
}
