<?php

use Illuminate\Database\Seeder;

class RentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rent')->truncate();
        DB::table('rent')->insert([
            [
                'rentDate'     => \Carbon\Carbon::now(),
                'dueDate'      => \Carbon\Carbon::now()->addDays(7),
                'borrower_id'  => 2,
                'admin_id'     => 1,
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ]
        ]);
    }
}
