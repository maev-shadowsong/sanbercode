<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'email'     => 'admin@example.com',
                'name'      => 'Admin',
                'password'  => bcrypt('admin123'),
                'role_id'     => '2',
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ],
            [
                'email'     => 'sapi@example.com',
                'name'      => 'sapi',
                'password'  => bcrypt('user123'),
                'role_id'     => '1',
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ]
        ]);
    }
}
