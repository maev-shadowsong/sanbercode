<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->truncate();
        DB::table('books')->insert([
            [
                'kode'     => 'XIII/Lightning',
                'judul'      => 'Binding of Lightning',
                'pengarang'  => 'Tanabaka',
                'tahun_terbit'     => '2013',
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ],
            [
                'kode'     => 'VII/Sephiroth',
                'judul'      => 'One Winged Angel',
                'pengarang'  => 'Tiroshi',
                'tahun_terbit'     => '1997',
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ]
        ]);
    }
}
