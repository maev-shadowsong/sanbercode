<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $table = 'rent';
    protected $fillable = ['rentDate', 'dueDate', 'returnDate', 'onDate', 'borrower_id', 'admin_id'];

    public function details()
    {
    	return $this->hasMany(RentDetail::class);
    }

    public function admin()
    {
    	return $this->belongsTo(User::class);
    }

    public function borrower()
    {
    	return $this->belongsTo(User::class);
    }
}
