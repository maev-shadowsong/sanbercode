<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['alpha_num','required','min:5','max:30','unique:users,name'],
            'email'=>['email','required','unique:users,email'],
            'password'=>['required','min:6'],
            'nim'=>['numeric','unique:users,nim','min:6'],
            'fakultas'=>['alpha','min:6'],
            'jurusan'=>['alpha','min:6'],
            'no_hp'=>['numeric','min:6','unique:users,no_hp'],
            'no_wa'=>['numeric','min:6','unique:users,no_wa']
        ];
    }
}
