<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterAuthRequest;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    /**
     * @var bool
     */
    public $loginAfterSignUp = true;

    /**
     * @SWG\Post(
     *   path="/api/login",
     *   summary="Post Login",
     *   operationId="login",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *	 @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="User email used to create account.",
     *      required=true,
     *      @SWG\Schema(
     *          @SWG\Property(property="email", type="string", example="admin@example.com"),
     *          @SWG\Property(property="password", type="string", example="admin123")
     *      ),
     *   )
     * )
     */
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }

    /**
     * @SWG\Post(
     *   path="/api/register",
     *   summary="Post Register",
     *   operationId="register",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
	 *	 @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      description="User email used to create account.",
     *      required=true,
     *      @SWG\Schema(
     *          @SWG\Property(property="email", type="string", example="user123@example.com"),
     *          @SWG\Property(property="password", type="string", example="admin123"),
     *          @SWG\Property(property="name", type="string", example="user123"),
     *          @SWG\Property(property="nim", type="string", example="123456789"),
     *          @SWG\Property(property="fakultas", type="string", example="peternakan"),
     *          @SWG\Property(property="jurusan", type="string", example="nuyul"),
     *          @SWG\Property(property="no_hp", type="string", example="012345678"),
     *          @SWG\Property(property="no_wa", type="string", example="012345678")
     *      ),
     *   )
     * )
     */
    public function registrasi(RegisterRequest $request)
    {
        // request()->validate([
        //     'name'=>['alpha_num','required','min:5','max:30','unique:users,name'],
        //     'email'=>['email','required','unique:users,email'],
        //     'password'=>['required','min:6'],
        //     'nim'=>['numeric','unique:users,nim','min:6'],
        //     'fakultas'=>['alpha','min:6'],
        //     'jurusan'=>['alpha','min:6'],
        //     'no_hp'=>['numeric','min:6','unique:users,no_hp'],
        //     'no_wa'=>['numeric','min:6','unique:users,no_wa']
        // ]);


        // User::create([
        //     'name'=>request('name'),
        //     'email'=>request('email'),
        //     'password'=>request('password'),
        //     'nim'=>request('nim'),
        //     'fakultas'=>request('fakultas'),
        //     'jurusan'=>request('jurusan'),
        //     'no_hp'=>request('no_hp'),
        //     'no_wa'=>request('no_wa')
        // ]);

        // return response('u r registerd');

        $daftar = new User();
        $daftar->name = $request->name;
        $daftar->email = $request->email;
        $daftar->password = bcrypt($request->password);
        $daftar->nim = $request->nim;
        // $daftar->role_id = $request->role_id;
        $daftar->fakultas = $request->fakultas;
        $daftar->jurusan = $request->jurusan;
        $daftar->no_hp = $request->no_hp;
        $daftar->no_wa = $request->no_wa;
        $daftar->save();


        if ($daftar) {
            return response()->json([
                'success' => true,
                'data' => $daftar
            ], 200);
        }
    }

    public function profil()
    {

        $response = [
            "success" => true,
            "data" => Auth::user()
        ];

        return response()
            ->json($response);
    }
}
