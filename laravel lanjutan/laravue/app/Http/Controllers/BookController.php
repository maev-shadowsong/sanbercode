<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/api/book",
     *   summary="Get Books",
     *   operationId="Book",
     *   security={{"Bearer":{}}},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     * )
     */
    public function index()
    {
        $data = Book::all();

        if(empty($data))
        {
            return response()->json([
                'status' => false,
                'message' => 'books not found'
            ], 200);

        }

        return response()->json([
            'status' => true,
            'message' => 'get books',
            'data' => $data
        ], 200);
    }
}
