<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentDetail extends Model
{
    protected $table = 'rent_detail';
    protected $fillable = ['rent_id', 'book_id'];

    public function rentId()
    {
    	return $this->belongsTo(Rent::class);
    }

    public function book()
    {
    	return $this->belongsTo(Book::class);
    }
}
