<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
	protected $fillable = ['judul', 'kode', 'pengarang', 'tahun_terbit'];

    public function rentDetail()
    {
    	return $this->hasMany(RentDetail::class);
    }
}
