<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'email', 'fakultas', 'password', 'jurusan', 'no_hp', 'no_wa'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
