<?php

use Illuminate\Database\Seeder;

class RentDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rent_detail')->truncate();
        DB::table('rent_detail')->insert([
            [
                'rent_id'     => 1,
                'book_id'      => 1,
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ],
            [
                'rent_id'     => 1,
                'book_id'      => 2,
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now(),
            ]
        ]);
    }
}
