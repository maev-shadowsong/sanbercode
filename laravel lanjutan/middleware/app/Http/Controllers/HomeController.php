<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function adminOnly()
    {
        return view('admin');
    }

    public function superAdminOnly()
    {
        return view('superAdmin');
    }

    public function guestOnly()
    {
        return view('guest');
    }
}
