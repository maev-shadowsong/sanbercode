<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isAdmin() && $request->is('admin') || $request->is("user")){
            return $next($request);
        }
        else if(Auth::user()->isSuperAdmin() && $request->is("superAdmin") || $request->is('admin') || $request->is("user")){
            return $next($request);
        }
        else if(Auth::user()->isUser() && $request->is("user")){
            return $next($request);
        }
        abort(403);
    }

    
}
