<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");

    echo $sheep->nama . '<br>'; // "shaun"
    echo $sheep->legs. '<br>'; // 2
    echo true === (bool)$sheep->cold_blooded ? 'True<br>' : 'False<br>';

    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    $kodok->jump(). '<br>' ; // "hop hop"

?>