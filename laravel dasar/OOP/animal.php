<?php

    class Animal{
        public $nama;
        public $cold_blooded;
        public $legs ;

        public function __construct($name)
        {
            $this->nama = $name;
            $this->legs = 2;
            $this->cold_blooded = false;
        }

    }