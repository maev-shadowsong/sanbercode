<?php
    require_once('animal.php');

    class Frog extends Animal{

        public function __construct($name)
        {
            $this->nama = $name;
            $this->legs = 4;
            $this->cold_blooded = false;
        }

        public function jump()
        {
            echo "hop hop<br>";
        }

    }