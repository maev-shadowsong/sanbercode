<?php
function tentukan_deret_geometri($arr) {
    $diff = 0;
    $geometri = false;
    $i=0;
    while ($i<count($arr)-1)
        {
            $i++;
            if($diff==0){
                $diff = $arr[$i]/$arr[$i-1];
            } else if($diff == $arr[$i]/$arr[$i-1]){
                $geometri = true;
            } else {
                $geometri = false;
            }
        }
    if($geometri==true)
        return "merupakan deret geometri<br>";
    return "<br>bukan merupakan deret geometri<br>";
}
//TEST CASES
echo tentukan_deret_geometri([1, 3, 9, 27, 81]); // true
echo tentukan_deret_geometri([2, 4, 8, 16, 32]); // true
echo tentukan_deret_geometri([2, 4, 6, 8]); // false
echo tentukan_deret_geometri([2, 6, 18, 54]); // true
echo tentukan_deret_geometri([1, 2, 3, 4, 7, 9]); //false
?>