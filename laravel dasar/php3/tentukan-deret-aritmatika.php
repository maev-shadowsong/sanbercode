<?php
function tentukan_deret_aritmatika($arr) {
    $diff = 0;
    $aritmatika = false;
    $i=0;
    while ($i<count($arr)-1)
        {
            $i++;
            if($diff==0){
                $diff = $arr[$i]-$arr[$i-1];
            } else if($diff == $arr[$i]-$arr[$i-1]){
                $aritmatika = true;
            } else {
                $aritmatika = false;
            }
        }
    if($aritmatika==true)
        return "merupakan deret aritmatika<br>";
    return "<br>bukan merupakan deret aritmatika<br>";
}

// TEST CASES
echo tentukan_deret_aritmatika([1, 2, 3, 4, 5, 6]);// true
echo tentukan_deret_aritmatika([2, 4, 6, 12, 24]);// false
echo tentukan_deret_aritmatika([2, 4, 6, 8]); //true
echo tentukan_deret_aritmatika([2, 6, 18, 54]);// false
echo tentukan_deret_aritmatika([1, 2, 3, 4, 7, 9]);// false
?>