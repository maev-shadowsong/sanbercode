<?php
function pasangan_terbesar($angka){
    $biggest =0;
    for ($i=0; $i <strlen($angka)-1 ; $i++) { 
        if(substr($angka,$i,2)>$biggest){
            $biggest = substr($angka,$i,2);
        }
    }
    return "Pasangan bilangan terbesar dari $angka adalah $biggest <br>";
}

// TEST CASES
echo pasangan_terbesar(641573); // 73
echo pasangan_terbesar(12783456); // 83
echo pasangan_terbesar(910233); // 91
echo pasangan_terbesar(71856421); // 85
echo pasangan_terbesar(79918293); // 99

?>