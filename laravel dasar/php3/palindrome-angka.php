<?php

function palindrome_angka($angka) {
    echo "Starting with ".$angka;
    $palindrom = false;
    while($palindrom==false){
        $angka++;    
        $i = 0;
        $j = strlen($angka)-1;
        while($j>=$i){        
            if(strval($angka)[$i] !== strval($angka)[$j]){
                break;
            } else if($i == $j || $j-$i==1){
                $palindrom = true;
                break;
            }
            $j--;
            $i++;            
        }
        if($j==0 || $palindrom == true)
            break;
    }
    echo " got $angka as true palindrome<br>";
}

// TEST CASES
echo palindrome_angka(8); // 9
echo palindrome_angka(10); // 11
echo palindrome_angka(117); // 121
echo palindrome_angka(175); // 181
echo palindrome_angka(1000); // 1001

?>