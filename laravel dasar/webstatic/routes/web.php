<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcomeCustom');
});

// Auth::routes();

Route::get('/register', 'RegisterController@index')->name('register');
Route::post('/registerPost', 'RegisterController@postRegister')->name('registerPost');

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'role'])->group(function(){
    Route::get('/admin', 'HomeController@adminOnly')->name('admin');
    Route::get('/superAdmin', 'HomeController@superAdminOnly')->name('super');
    Route::get('/user', 'HomeController@guestOnly')->name('user');
});
